<?php

namespace Drupal\soft_translations_fallback;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\TypedData\TranslatableInterface as TranslatableDataInterface;

/**
 * Decorates the entity repository service to be soft-translations-aware.
 */
final class SoftTranslationsAwareEntityRepository implements EntityRepositoryInterface {

  /**
   * The inner entity repository service.
   *
   * @var \Drupal\Core\Entity\EntityRepositoryInterface
   */
  protected EntityRepositoryInterface $inner;

  /**
   * The language manager service.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected LanguageManagerInterface $languageManager;

  /**
   * Constructs a new SoftTranslationsAwareEntityRepository.
   */
  public function __construct(EntityRepositoryInterface $entity_repository, LanguageManagerInterface $language_manager) {
    $this->inner = $entity_repository;
    $this->languageManager = $language_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function getTranslationFromContext(EntityInterface $entity, $langcode = NULL, $context = []) {
    $translation = $this->inner->getTranslationFromContext($entity, $langcode, $context);

    if (empty($langcode)) {
      $langcode = $this->languageManager->getCurrentLanguage(LanguageInterface::TYPE_CONTENT)->getId();
    }

    if ($entity instanceof TranslatableDataInterface && !$entity->isNew()) {
      /** @var \Drupal\soft_translations\SoftTranslationsManager $soft_translations_manager */
      $soft_translations_manager = \Drupal::service('soft_translations.manager');
      $soft_translations = $soft_translations_manager->getSoftTranslations($entity, TRUE);
      if (!empty($soft_translations[$langcode])) {
        return $soft_translations[$langcode];
      }
    }

    return $translation;
  }

  /**
   * {@inheritdoc}
   */
  public function loadEntityByUuid($entity_type_id, $uuid) {
    return $this->inner->loadEntityByUuid($entity_type_id, $uuid);
  }

  /**
   * {@inheritdoc}
   */
  public function loadEntityByConfigTarget($entity_type_id, $target) {
    return $this->inner->loadEntityByConfigTarget($entity_type_id, $target);
  }

  /**
   * {@inheritdoc}
   */
  public function getActive($entity_type_id, $entity_id, array $contexts = NULL) {
    return $this->inner->getActive($entity_type_id, $entity_id, $contexts);
  }

  /**
   * {@inheritdoc}
   */
  public function getActiveMultiple($entity_type_id, array $entity_ids, array $contexts = NULL) {
    return $this->inner->getActiveMultiple($entity_type_id, $entity_ids, $contexts);
  }

  /**
   * {@inheritdoc}
   */
  public function getCanonical($entity_type_id, $entity_id, array $contexts = NULL) {
    return $this->inner->getCanonical($entity_type_id, $entity_id, $contexts);
  }

  /**
   * {@inheritdoc}
   */
  public function getCanonicalMultiple($entity_type_id, array $entity_ids, array $contexts = NULL) {
    return $this->inner->getCanonicalMultiple($entity_type_id, $entity_ids, $contexts);
  }

}
