<?php

namespace Drupal\Tests\soft_translations_fallback\Functional;

use Drupal\node\Entity\Node;
use Drupal\Tests\soft_translations\Functional\SoftTranslationsTestBase;

/**
 * Tests the translation fallback mechanism on soft translations.
 *
 * @group soft_translations
 */
class SoftTranslationsFallbackTest extends SoftTranslationsTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'node',
    'field',
    'language',
    'content_translation',
    'soft_translations',
    'soft_translations_fallback',
    'replicate',
    'system',
    'path',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();
    $this->initializeNodeSetup(['es']);
  }

  /**
   * Checks that the entity.repository service can return soft translations.
   */
  public function testTranslationFromContextFallback() {
    $alias = '/common-alias';
    [$node1, $node2] = $this->createSoftTranslationNodesForLanguages(['es'], $alias);
    $entity_repository = \Drupal::service('entity.repository');
    $translation = $entity_repository->getTranslationFromContext($node1, 'es');
    $this->assertEquals($node2->id(), $translation->id());
  }

  /**
   * Check that the decorated entity.repository can handle unsaved nodes.
   */
  public function testUnsavedEntity() {
    $node = Node::create(['title' => 'Test unsaved', 'type' => 'test_content_type1']);
    /** @var \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository */
    $entity_repository = \Drupal::service('entity.repository');
    $translation = $entity_repository->getTranslationFromContext($node, 'es');
    $this->assertEquals($node, $translation);
  }

}
