<?php

namespace Drupal\Tests\soft_translations\Kernel;

use Drupal\KernelTests\KernelTestBase;
use Drupal\soft_translations_composite_test\Entity\EntityTestComposite;

/**
 * Test entities without a canonical link template.
 *
 * @group soft_translations
 */
class CompositeEntityTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'language',
    'user',
    'content_translation',
    'path_alias',
    'soft_translations',
    'entity_test',
    'soft_translations_composite_test',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->installEntitySchema('entity_test_composite');
  }

  /**
   * Check that entities without a canonical link template are handled.
   */
  public function testUndefinedLinkTemplateExceptionDoesNotGetThrown() {
    $entity = EntityTestComposite::create(['title' => 'test']);
    $entity->save();
    $this->assertEmpty($this->container->get('soft_translations.manager')->getSoftTranslations($entity));
  }

}
