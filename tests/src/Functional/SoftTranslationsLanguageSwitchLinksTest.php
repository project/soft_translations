<?php

namespace Drupal\Tests\soft_translations\Functional;

use Drupal\Core\Language\LanguageInterface;

/**
 * Tests the language switch links on soft translations.
 *
 * @group soft_translations
 */
class SoftTranslationsLanguageSwitchLinksTest extends SoftTranslationsTestBase {

  /**
   * {@inheritdoc}
   */
  public static $modules = [
    'block',
    'node',
    'field',
    'language',
    'content_translation',
    'soft_translations',
    'replicate',
    'system',
    'path',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * An admin user.
   *
   * @var \Drupal\user\Entity\User
   */
  protected $adminUser;

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    // Place some blocks to make our lives easier down the road.
    $this->drupalPlaceBlock('system_breadcrumb_block');
    $this->drupalPlaceBlock('local_tasks_block');
    $this->drupalPlaceBlock('local_actions_block');
    $this->drupalPlaceBlock('page_title_block');
    $this->drupalPlaceBlock('language_block:' . LanguageInterface::TYPE_INTERFACE);

    $this->adminUser = $this->drupalCreateUser([
      'administer languages',
      'administer content translation',
      'create content translations',
      'translate any entity',
      'access content overview',
      'administer content types',
      'administer nodes',
      'bypass node access',
    ]);
  }

  /**
   * Checks that we inject soft translations in the language switcher.
   */
  public function testSoftTranslationsLangSwitchLinks() {
    $assert_session = $this->assertSession();

    $this->drupalLogin($this->adminUser);

    $this->initializeNodeSetup(['es', 'fr']);
    $alias = '/common-alias';
    [$node_en, $node_es] = $this->createSoftTranslationNodesForLanguages(['es'], $alias);
    $this->assertTrue($node_en->id() <> $node_es->id());

    // Visit each of the nodes and check the language switch links.
    foreach ([$node_en, $node_es] as $node) {
      $this->drupalGet($node->toUrl());

      $en_link = $assert_session->elementExists('css', '.language-switcher-language-url a[hreflang="en"]');
      $this->assertSame("node/{$node_en->id()}", $en_link->getAttribute('data-drupal-link-system-path'));
      $this->assertStringContainsString('/common-alias', $en_link->getAttribute('href'));

      $fr_link = $assert_session->elementExists('css', '.language-switcher-language-url a[hreflang="fr"]');
      $this->assertSame("node/{$node_en->id()}", $fr_link->getAttribute('data-drupal-link-system-path'));
      $this->assertStringNotContainsString('common-alias', $fr_link->getAttribute('href'));

      $es_link = $assert_session->elementExists('css', '.language-switcher-language-url a[hreflang="es"]');
      $this->assertSame("node/{$node_es->id()}", $es_link->getAttribute('data-drupal-link-system-path'));
      $this->assertStringContainsString('/es/common-alias', $es_link->getAttribute('href'));
    }
  }

}
