<?php

namespace Drupal\Tests\soft_translations\Functional;

/**
 * Tests the additional overview table for soft translations.
 *
 * @group soft_translations
 */
class SoftTranslationsOverviewTest extends SoftTranslationsTestBase {

  /**
   * {@inheritdoc}
   */
  public static $modules = [
    'block',
    'node',
    'field',
    'language',
    'content_translation',
    'soft_translations',
    'replicate',
    'system',
    'path',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * An admin user.
   *
   * @var \Drupal\user\Entity\User
   */
  protected $adminUser;

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    // Place some blocks to make our lives easier down the road.
    $this->drupalPlaceBlock('system_breadcrumb_block');
    $this->drupalPlaceBlock('local_tasks_block');
    $this->drupalPlaceBlock('local_actions_block');
    $this->drupalPlaceBlock('page_title_block');

    $this->adminUser = $this->drupalCreateUser([
      'administer languages',
      'administer content translation',
      'create content translations',
      'translate any entity',
      'access content overview',
      'administer content types',
      'administer nodes',
      'bypass node access',
    ]);
  }

  /**
   * Checks that the translation overview correctly displays soft translations.
   */
  public function testSoftTranslationsOverview() {
    $session = $this->getSession();
    $page = $session->getPage();
    $assert_session = $this->assertSession();

    $this->drupalLogin($this->adminUser);

    $this->initializeNodeSetup(['es', 'fr']);
    [$node1, $node2, $node3] = $this->createSoftTranslationNodesForLanguages(['es', 'fr']);

    // Click the "Translate" tab on node1, verify the additional table is there.
    $this->drupalGet($node1->toUrl());
    $translate_tab = $assert_session->elementExists('css', 'ul li a[href$="/translations"]');
    $translate_tab->click();

    // We have no (real) translations and a few soft-translations.
    $rows = $page->findAll('css', 'table#edit-languages tbody tr');
    foreach ($rows as $row) {
      $lang_td = $row->find('css', 'td:nth-child(2)');
      if ($lang_td->getText() === 'English (Original language)') {
        $this->assertStringContainsString($node1->label(), $row->find('css', 'td:nth-child(3)')->getText());
      }
      elseif ($lang_td->getText() === 'French') {
        $this->assertStringContainsString('n/a', $row->find('css', 'td:nth-child(3)')->getHtml());
      }
    }

    $assert_session->pageTextContains('Related Localized Content (Soft Translations)');
    $assert_session->pageTextContains('This page has other content that also exists at the same path in a different language. These pages are not one-to-one translations, but likely contain similar content that is localized to a particular region/language');

    $rows = $page->findAll('css', 'table.soft-translations-overview-table tbody tr');
    foreach ($rows as $row) {
      $lang_td = $row->find('css', 'td:nth-child(1)');
      if ($lang_td->getText() === 'English (Original language)') {
        $this->fail('We should not have an English soft translation!');
      }
      elseif ($lang_td->getText() === 'Spanish') {
        $this->assertStringContainsString($node2->label(), $row->find('css', 'td:nth-child(2)')->getText());
      }
      elseif ($lang_td->getText() === 'French') {
        $this->assertStringContainsString($node3->label(), $row->find('css', 'td:nth-child(2)')->getText());
      }
    }

    // A node that only has real translations doesn't get the table.
    $node4 = $this->drupalCreateNode([
      'type' => 'test_content_type1',
      'title' => 'Node 4 - EN',
      'langcode' => 'en',
      'path' => '/another-alias',
    ]);
    $node4->save();
    $node4->addTranslation('es', [
      'title' => 'Node 4 - ES',
    ] + $node4->toArray());
    $node4->addTranslation('fr', [
        'title' => 'Node 4 - FR',
      ] + $node4->toArray());
    $node4->save();
    $this->drupalGet($node4->toUrl());
    $translate_tab = $assert_session->elementExists('css', 'ul li a[href$="/translations"]');
    $translate_tab->click();
    $assert_session->pageTextNotContains('Related Localized Content (Soft Translations)');
    $assert_session->elementNotExists('css', 'table.soft-translations-overview-table');
  }

}
