<?php

namespace Drupal\Tests\soft_translations\Functional;

/**
 * Tests the hreflang on soft translations.
 *
 * @group soft_translations
 */
class SoftTranslationsHreflangTest extends SoftTranslationsTestBase {

  /**
   * {@inheritdoc}
   */
  public static $modules = [
    'block',
    'node',
    'field',
    'language',
    'content_translation',
    'soft_translations',
    'replicate',
    'system',
    'path',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * An admin user.
   *
   * @var \Drupal\user\Entity\User
   */
  protected $adminUser;

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    // Place some blocks to make our lives easier down the road.
    $this->drupalPlaceBlock('system_breadcrumb_block');
    $this->drupalPlaceBlock('local_tasks_block');
    $this->drupalPlaceBlock('local_actions_block');
    $this->drupalPlaceBlock('page_title_block');

    $this->adminUser = $this->drupalCreateUser([
      'administer languages',
      'administer content translation',
      'create content translations',
      'translate any entity',
      'access content overview',
      'administer content types',
      'administer nodes',
      'bypass node access',
    ]);
  }

  /**
   * Checks that nodes display correctly alternate links to soft translations.
   */
  public function testHreflangSoftTranslations() {
    $assert_session = $this->assertSession();

    $this->drupalLogin($this->adminUser);

    $this->initializeNodeSetup(['es', 'fr']);
    $alias = '/common-alias';
    [$node1, $node2, $node3] = $this->createSoftTranslationNodesForLanguages(['es', 'fr'], $alias);

    // Don't forget tests on drupal.org are run in a subdirectory.
    $base_path = base_path();
    $host = \Drupal::request()->getSchemeAndHttpHost();
    $config = \Drupal::config('language.negotiation')->get('url');

    $this->drupalGet('/node/' . $node1->id());
    $fr_link = $assert_session->elementExists('css', 'head link[rel="alternate"][hreflang="fr"]');
    $lang_prefix = $config['prefixes']['fr'];
    $this->assertSame($host . $base_path . $lang_prefix . $alias, $fr_link->getAttribute('href'));
    $es_link = $assert_session->elementExists('css', 'head link[rel="alternate"][hreflang="es"]');
    $lang_prefix = $config['prefixes']['es'];
    $this->assertSame($host . $base_path . $lang_prefix . $alias, $es_link->getAttribute('href'));

    $this->drupalGet('/node/' . $node2->id());
    $fr_link = $assert_session->elementExists('css', 'head link[rel="alternate"][hreflang="fr"]');
    $lang_prefix = $config['prefixes']['fr'];
    $this->assertSame($host . $base_path . $lang_prefix . $alias, $fr_link->getAttribute('href'));
    $en_link = $assert_session->elementExists('css', 'head link[rel="alternate"][hreflang="en"]');
    $lang_prefix = $config['prefixes']['en'];
    $this->assertSame(str_replace('http:/', 'http://', str_replace('//', '/', $host . $base_path . $lang_prefix . $alias)), $en_link->getAttribute('href'));
  }

}
