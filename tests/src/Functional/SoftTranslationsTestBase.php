<?php

namespace Drupal\Tests\soft_translations\Functional;

use Drupal\language\Entity\ConfigurableLanguage;
use Drupal\Tests\BrowserTestBase;

/**
 * Base class to help test soft translations.
 */
abstract class SoftTranslationsTestBase extends BrowserTestBase {

  /**
   * Initialize a node-based setup for common testing operations.
   *
   * This will:
   *  - Create configurable languages for each langcode passed in.
   *  - Create a content type with the given name.
   *  - Make that content type translatable.
   *
   * @param array $langcodes
   *   An indexed array of langcodes to create.
   * @param string $content_type_id
   *   (optional) The machine name of the content type to create. Defaults to
   *   'test_content_type1'.
   */
  protected function initializeNodeSetup(array $langcodes, $content_type_id = 'test_content_type1') {
    // Create the requested languages.
    foreach ($langcodes as $langcode) {
      ConfigurableLanguage::createFromLangcode($langcode)->save();
    }

    // Create a content type.
    $content_type = $this->drupalCreateContentType([
      'type' => $content_type_id,
      'name' => 'Test Content Type 1',
    ]);
    $content_type->save();

    // Enable translation for it.
    $anonymous = FALSE;
    if (empty($this->loggedInUser)) {
      $anonymous = TRUE;
      $this->drupalLogin($this->rootUser);
    }
    $this->drupalGet('/admin/config/regional/content-language');
    $edit = [
      'entity_types[node]' => TRUE,
      "settings[node][{$content_type_id}][fields][title]" => TRUE,
      "settings[node][{$content_type_id}][translatable]" => TRUE,
    ];
    $this->submitForm($edit, 'Save configuration');
    $this->assertSession()->pageTextContains('Settings successfully updated');

    if ($anonymous) {
      $this->drupalLogout();
    }
  }

  /**
   * Create a few soft-translation nodes.
   *
   * @param array $langcodes
   *   An indexed array of langcodes to create nodes in.
   * @param string $common_alias
   *   (optional) The alias to use. Defaults to '/common-alias'.
   * @param string $content_type_id
   *   (optional) The machine name of the content type to create. Defaults to
   *   'test_content_type1'.
   *
   * @return array
   *   An indexed array of created node objects (already saved). The first
   *   element of the array will be in English, and the other elements will
   *   be each in the langcode that was passed in in $langcodes, in the same
   *   order.
   */
  protected function createSoftTranslationNodesForLanguages(array $langcodes, $common_alias = '/common-alias', $content_type_id = 'test_content_type1') {
    $nodes = [];
    // The first node will always be in English.
    $i = 0;
    $nodes[$i] = $this->drupalCreateNode([
      'type' => $content_type_id,
      'title' => "Node $i - EN",
      'langcode' => 'en',
      'path' => $common_alias,
    ]);
    $nodes[$i]->save();

    // Create additional ones for each language passed in.
    foreach ($langcodes as $langcode) {
      $i++;
      $nodes[$i] = $this->drupalCreateNode([
        'type' => $content_type_id,
        'title' => "Node $i - " . strtoupper($langcode),
        'langcode' => $langcode,
        'path' => $common_alias,
      ]);
      $nodes[$i]->save();
    }
    return $nodes;
  }

}
