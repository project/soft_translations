<?php

namespace Drupal\Tests\soft_translations\Functional;

/**
 * Tests the ability to split off translations into standalone entities.
 *
 * @group soft_translations
 */
class SoftTranslationsSplitOffTest extends SoftTranslationsTestBase {

  /**
   * {@inheritdoc}
   */
  public static $modules = [
    'block',
    'node',
    'field',
    'language',
    'content_translation',
    'soft_translations',
    'replicate',
    'system',
    'path',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * An admin user.
   *
   * @var \Drupal\user\Entity\User
   */
  protected $adminUser;

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    // Place some blocks to make our lives easier down the road.
    $this->drupalPlaceBlock('system_breadcrumb_block');
    $this->drupalPlaceBlock('local_tasks_block');
    $this->drupalPlaceBlock('local_actions_block');
    $this->drupalPlaceBlock('page_title_block');

    $this->adminUser = $this->drupalCreateUser([
      'administer languages',
      'administer content translation',
      'create content translations',
      'translate any entity',
      'access content overview',
      'administer content types',
      'administer nodes',
      'bypass node access',
      'split off soft translations',
    ]);
  }

  /**
   * Checks that we can split off basic node translations.
   */
  public function testSoftTranslationsNodeSplitBasic() {
    $session = $this->getSession();
    $page = $session->getPage();
    $assert_session = $this->assertSession();

    $this->drupalLogin($this->adminUser);

    $this->initializeNodeSetup(['es', 'fr']);
    // Create a node.
    $node1 = $this->drupalCreateNode([
      'type' => 'test_content_type1',
      'title' => 'Node 1 - EN',
      'langcode' => 'en',
      'path' => '/foo-bar',
    ]);
    // Give it a couple translations.
    $node1->addTranslation('es', ['title' => 'Node 1 - ES'] + $node1->toArray());
    $node1->addTranslation('fr', ['title' => 'Node 1 - FR'] + $node1->toArray());
    $node1->save();

    // Click the "Translate" tab on node1, verify we don't have soft
    // translations..
    $this->drupalGet($node1->toUrl());
    $translate_tab = $assert_session->elementExists('css', 'ul li a[href$="/translations"]');
    $translate_tab->click();
    $assert_session->pageTextNotContains('Related Localized Content (Soft Translations)');
    $assert_session->elementNotExists('css', 'table.soft-translations-overview-table');

    // Check that the French row now has an operation button to split off
    // the translation.
    $rows = $page->findAll('css', 'table tbody tr');
    $lang_td = FALSE;
    foreach ($rows as $row) {
      $lang_td = $row->find('css', 'td:nth-child(1)');
      if ($lang_td->getText() === 'French') {
        break;
      }
    }
    if (empty($lang_td)) {
      $this->fail('Did not find a French row in the translations overview table.');
    }
    $split_fr_link = $row->find('css', 'ul.dropbutton a[href*="translations/st-split"]');
    $this->assertNotNull($split_fr_link);
    // Verify the English original does not have the same link.
    $lang_td = FALSE;
    foreach ($rows as $row) {
      $lang_td = $row->find('css', 'td:nth-child(1)');
      if ($lang_td->getText() === 'English (Original language)') {
        break;
      }
    }
    if (empty($lang_td)) {
      $this->fail('Did not find an English row in the translations overview table.');
    }
    $this->assertNull($row->find('css', 'ul.dropbutton a[href$="/translations/st-split/fr?entity_type=node"]'));

    // Click the link to split the translation off.
    $split_fr_link->click();
    $assert_session->pageTextContains('Split off Node 1 - EN');
    $assert_session->pageTextContains('Splitting a translation will make it a stand-alone piece of content so that its content may differ from the original language. This allows more extensive customization but makes automated translation workflows potentially more complex. Combining a splitted page back into a translation must be done manually, so this action is not easily reversible. Proceed with splitting off this translation?');
    // Cancel and verify we are back at the overview page.
    $page->clickLink('Cancel');
    $assert_session->pageTextContains('Translations of Node 1 - EN');
    $assert_session->elementExists('css', 'ul.dropbutton a[href*="/translations/st-split/fr"]')
      ->click();
    $assert_session->pageTextContains('Split off Node 1 - EN');
    $page->pressButton('Split Translation');
    $assert_session->pageTextContains('Translation Node 1 - FR has been split off into a standalone entity');

    $node_storage = \Drupal::entityTypeManager()->getStorage('node');
    $results = $node_storage->loadByProperties(['title' => 'Node 1 - FR']);
    /** @var \Drupal\node\NodeInterface $node_fr */
    $node_fr = reset($results);

    // Check some values at the API level.
    /** @var \Drupal\node\NodeInterface $node1 */
    $node1 = $node_storage->loadUnchanged($node1->id());
    // These are actual different nodes.
    $this->assertTrue($node1->id() <> $node_fr->id());
    // The new one has the correct title, alias, etc.
    $this->assertSame('Node 1 - FR', $node_fr->getTitle());
    $this->assertSame($node1->path->alias, $node_fr->path->alias);
    // Their original languages are correctly defined.
    $this->assertTrue($node1->getUntranslated()->language()->getId() === 'en');
    $this->assertTrue($node_fr->getUntranslated()->language()->getId() === 'fr');
    // Check the original translation isn't there anymore.
    $this->assertFalse($node1->hasTranslation('fr'));

    // Check that we see the same on screen.
    $rows = $page->findAll('css', 'table:not(.soft-translations-overview-table) tbody tr');
    $lang_td = FALSE;
    foreach ($rows as $row) {
      $lang_td = $row->find('css', 'td:nth-child(1)');
      if ($lang_td->getText() === 'French') {
        break;
      }
    }
    if (empty($lang_td)) {
      $this->fail('Did not find a French row in the translations overview table.');
    }
    $this->assertTrue($row->find('css', 'td:nth-child(4)')->getText() === 'Not translated');
    //$this->assertStringContainsString('Not translated', $row->getText());
    // Now none of the links on the first table offer to split anything.
    $assert_session->elementNotExists('css', 'ul.dropbutton a[href*="/translations/st-split/fr"]');

    // Check we now have a soft-translation French node in there.
    $assert_session->pageTextContains('Related Localized Content (Soft Translations)');
    $rows = $page->findAll('css', 'table.soft-translations-overview-table tbody tr');
    $there_is_soft_fr = FALSE;
    foreach ($rows as $row) {
      $lang_td = $row->find('css', 'td:nth-child(1)');
      if ($lang_td->getText() === 'French') {
        $there_is_soft_fr = TRUE;
        break;
      }
    }
    if (!$there_is_soft_fr) {
      $this->fail('Did not find a French soft translation.');
    }
    $this->assertSame('Node 1 - FR', $row->find('css', 'td:nth-child(2)')->getText());
  }

}
