<?php

namespace Drupal\Tests\soft_translations\Functional;

use Drupal\media\Entity\Media;
use Drupal\Tests\media\Traits\MediaTypeCreationTrait;

/**
 * Tests that we can identify soft translations through the API.
 *
 * @group soft_translations
 */
class SoftTranslationsApiTest extends SoftTranslationsTestBase {

  use MediaTypeCreationTrait;

  /**
   * {@inheritdoc}
   */
  public static $modules = [
    'block',
    'node',
    'field',
    'image',
    'media',
    'media_test_source',
    'language',
    'content_translation',
    'soft_translations',
    'replicate',
    'system',
    'path',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * An admin user.
   *
   * @var \Drupal\user\Entity\User
   */
  protected $adminUser;

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    // Place some blocks to make our lives easier down the road.
    $this->drupalPlaceBlock('system_breadcrumb_block');
    $this->drupalPlaceBlock('local_tasks_block');
    $this->drupalPlaceBlock('local_actions_block');
    $this->drupalPlaceBlock('page_title_block');

    $this->adminUser = $this->drupalCreateUser([
      'administer languages',
      'administer content translation',
      'create content translations',
      'translate any entity',
      'access content overview',
      'administer content types',
      'administer nodes',
      'bypass node access',
      'administer media',
      'administer media types',
      'update any media',
      'create media',
      'access media overview',
    ]);
  }

  /**
   * Tests that we can identify soft translations by calling the helper.
   */
  public function testSoftTranslationsApi() {
    $assert_session = $this->assertSession();

    $this->drupalLogin($this->adminUser);

    $this->initializeNodeSetup(['es', 'fr']);
    [$node1, $node2, $node3] = $this->createSoftTranslationNodesForLanguages(['es', 'fr']);

      /** @var \Drupal\soft_translations\SoftTranslationsManager $manager */
    $manager = \Drupal::service('soft_translations.manager');

    // Check soft translations of node 1.
    $soft_translations = $manager->getSoftTranslations($node1);
    $expected = [
      'es' => "/node/{$node2->id()}",
      'fr' => "/node/{$node3->id()}",
    ];
    $this->assertSame(ksort($expected), ksort($soft_translations));
    $soft_translations = $manager->getSoftTranslations($node1, TRUE);
    $expected = [
      'es' => $node2,
      'fr' => $node3,
    ];
    $this->assertSame(ksort($expected), ksort($soft_translations));

    // Check soft translations of node 2.
    $soft_translations = $manager->getSoftTranslations($node2);
    $expected = [
      'en' => "/node/{$node1->id()}",
      'fr' => "/node/{$node3->id()}",
    ];
    $this->assertSame(ksort($expected), ksort($soft_translations));
    $soft_translations = $manager->getSoftTranslations($node2, TRUE);
    $expected = [
      'en' => $node1,
      'fr' => $node3,
    ];
    $this->assertSame(ksort($expected), ksort($soft_translations));

    // Change one of the aliases, verify they no longer are considered a
    // soft translation.
    $node3->path = '/im-no-longer-common';
    $node3->save();
    $soft_translations = $manager->getSoftTranslations($node1);
    $expected = [
      'es' => "/node/{$node2->id()}",
    ];
    $this->assertSame(ksort($expected), ksort($soft_translations));
    $soft_translations = $manager->getSoftTranslations($node1, TRUE);
    $expected = [
      'es' => $node2,
    ];
    $this->assertSame(ksort($expected), ksort($soft_translations));

    // Verify the same works for media entities.
    $media_type = $this->createMediaType('test');
    $this->drupalGet('/admin/config/regional/content-language');
    $edit = [
      'entity_types[media]' => TRUE,
      'settings[media][' . $media_type->id() . '][fields][name]' => TRUE,
      'settings[media][' . $media_type->id() . '][translatable]' => TRUE,
    ];
    $this->submitForm($edit, 'Save configuration');
    $assert_session->pageTextContains('Settings successfully updated');
    $media1 = Media::create([
      'bundle' => $media_type->id(),
      'name' => 'Media One',
      'langcode' => 'en',
      'path' => '/common-media-alias',
    ]);
    $media1->save();
    $media2 = Media::create([
      'bundle' => $media_type->id(),
      'name' => 'Media Two',
      'langcode' => 'es',
      'path' => '/common-media-alias',
    ]);
    $media2->save();
    $media3 = Media::create([
      'bundle' => $media_type->id(),
      'name' => 'Media Three',
      'langcode' => 'fr',
      'path' => '/common-media-alias',
    ]);
    $media3->save();

    $soft_translations = $manager->getSoftTranslations($media1);
    $expected = [
      'es' => "/media/{$media2->id()}/edit",
      'fr' => "/media/{$media3->id()}/edit",
    ];
    $this->assertSame(ksort($expected), ksort($soft_translations));
    $soft_translations = $manager->getSoftTranslations($node1, TRUE);
    $expected = [
      'es' => $media2,
      'fr' => $media3,
    ];
    $this->assertSame(ksort($expected), ksort($soft_translations));
  }

}
