<?php

namespace Drupal\soft_translations_composite_test\Entity;

use Drupal\entity_test\Entity\EntityTest;

/**
 * Defines the test entity class.
 *
 * @ContentEntityType(
 *   id = "entity_test_composite",
 *   label = @Translation("Test composite entity"),
 *   base_table = "translatable_entity_test",
 *   data_table = "translatable_entity_test_data",
 *   entity_revision_parent_type_field = "parent_type",
 *   translatable = TRUE,
 *   content_translation_ui_skip = TRUE,
 *   handlers = {
 *     "access" = "Drupal\entity_test\EntityTestAccessControlHandler",
 *     "view_builder" = "Drupal\entity_test\EntityTestViewBuilder"
 *   },
 *   entity_keys = {
 *     "id" = "id",
 *     "uuid" = "uuid",
 *     "bundle" = "type",
 *     "label" = "name",
 *     "langcode" = "langcode",
 *   }
 * )
 */
class EntityTestComposite extends EntityTest {
}
