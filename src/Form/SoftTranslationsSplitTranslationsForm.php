<?php

namespace Drupal\soft_translations\Form;

use Drupal\Core\Entity\ContentEntityConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Confirmation form prior to splitting off a translation.
 */
class SoftTranslationsSplitTranslationsForm extends ContentEntityConfirmFormBase {

  /**
   * The route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * The replicator service.
   *
   * @var \Drupal\replicate\Replicator
   */
  protected $replicator;

  /**
   * The langcode of the translation about to be splitted.
   *
   * @var string
   */
  protected $langcode;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $form = parent::create($container);
    $form->replicator = $container->get('replicate.replicator');
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, RouteMatchInterface $route_match = NULL) {
    $this->routeMatch = $route_match;
    $this->langcode = $this->routeMatch->getParameter('language');
    $entity = $this->getEntity();

    // Prevent splitting this entity if it's used as the frontpage.
    // @todo If this becomes a mandatory feature, we will need to write
    // additional code to intercept inbound requests to `<front>` and map them
    // to each corresponding entity in the correct language 🙈.
    $frontpage_path = $this->config('system.site')->get('page.front');
    if ($frontpage_path === $entity->toUrl()->toString()) {
      return [
        '#markup' => $this->t('Splitting off the frontpage is not allowed.'),
      ];
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    /** @var \Drupal\Core\Entity\TranslatableInterface $entity */
    $entity = $this->getEntity();
    $entity_type = $entity->getEntityType();
    // We will create an entity from scratch based on this object's values in
    // the language we are interested in. By doing this we get combined fields
    // (both translated and untranslated values) into a single language, which
    // will also be set as default.
    $translation = $entity->getTranslation($this->langcode);
    $translated_title = $translation->label();
    $values = $translation->toArray();
    $keys_to_unset = [
      'id',
      'revision',
      'uuid',
    ];
    foreach ($keys_to_unset as $key) {
      $entity_key = $entity_type->getKey($key);
      if (!empty($entity_key)) {
        unset($values[$entity_key]);
      }
    }
    // If the entity has a path alias, make sure a new one is created for the
    // new replicated entity.
    unset($values['path'][0]['pid']);
    // Ensure the current user gets authorship of this entity.
    if (!empty($values['uid'])) {
      $values['uid'][0]['target_id'] = $this->currentUser()->id();
    }
    $values['default_langcode'][0]['value'] = TRUE;
    $new_entity = $this->entityTypeManager
      ->getStorage($entity_type->id())
      ->create($values);

    // Unfortunately we need to loop over all paragraphs and make sure we are
    // creating new instances of each, instead of reusing them directly.
    foreach ($new_entity->getFieldDefinitions() as $field_name => $field_definition) {
      if ($field_definition->getItemDefinition()->getSetting('target_type') == 'paragraph') {
        foreach ($new_entity->get($field_name) as $field_item) {
          $field_item->entity = $this->replicator->replicateEntity($field_item->entity);
        }
      }
    }

    $new_entity->setRevisionTranslationAffected(TRUE);
    $new_entity->save();

    // Remove the original translation.
    $entity->removeTranslation($this->langcode);
    $entity->setValidationRequired(FALSE);
    $entity->save();

    // Add the split entity to the form state storage, so it can be
    // accessed by other submit callbacks.
    $form_state->set('soft_translations_split_entity', $new_entity);

    $this->messenger()->addStatus($this->t('Translation %label has been split off into a standalone entity.', [
      '%label' => $translated_title,
    ]));
    $form_state->setRedirectUrl($this->getCancelUrl());
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    $entity = $this->getEntity();
    if ($entity->hasTranslation($this->langcode)) {
      $node = $entity->getTranslation($this->langcode);
    }
    return $this->t('Split off %title', ['%title' => $entity->label()]);
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->t('Splitting a translation will make it a stand-alone piece of content so that its content may differ from the original language. This allows more extensive customization but makes automated translation workflows potentially more complex. Combining a splitted page back into a translation must be done manually, so this action is not easily reversible. Proceed with splitting off this translation?');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Split Translation');
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    $entity = $this->getEntity();
    return Url::fromRoute('entity.' . $entity->getEntityTypeId() . '.content_translation_overview', [$entity->getEntityTypeId() => $entity->id()]);
  }

  /**
   * Determines which entity will be used by this form from a RouteMatch object.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match.
   * @param string $entity_type_id
   *   The entity type identifier.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   The entity object as determined from the passed-in route match.
   */
  public function getEntityFromRouteMatch(RouteMatchInterface $route_match, $entity_type_id) {
    if ($route_match->getRawParameter($entity_type_id) !== NULL) {
      $entity_id = $route_match->getParameter($entity_type_id);
    }
    if (!empty($entity_id)) {
      $entity = $this->entityTypeManager->getStorage($entity_type_id)->load($entity_id);
    }
    if (empty($entity)) {
      throw new \Exception("Could not load entity of type {$entity_type_id} with ID: {$entity_id}");
    }
    return $entity;
  }

}
