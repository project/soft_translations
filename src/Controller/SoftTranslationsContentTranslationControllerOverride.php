<?php

namespace Drupal\soft_translations\Controller;

use Drupal\content_translation\ContentTranslationManagerInterface;
use Drupal\content_translation\Controller\ContentTranslationController;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\Core\Entity\TranslatableInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Url;
use Drupal\soft_translations\SoftTranslationsManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Overridden class for entity translation controllers.
 */
class SoftTranslationsContentTranslationControllerOverride extends ContentTranslationController {

  /**
   * The soft translations manager.
   *
   * @var \Drupal\soft_translations\SoftTranslationsManager
   */
  protected $softTranslationsManager;

  /**
   * Class constructor.
   *
   * @param \Drupal\content_translation\ContentTranslationManagerInterface $manager
   *   A content translation manager instance.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager service.
   * @param \Drupal\soft_translations\SoftTranslationsManager $soft_translations_manager
   *   The soft translations manager.
   */
  public function __construct(ContentTranslationManagerInterface $manager, EntityFieldManagerInterface $entity_field_manager, SoftTranslationsManager $soft_translations_manager) {
    parent::__construct($manager, $entity_field_manager);
    $this->softTranslationsManager = $soft_translations_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('content_translation.manager'),
      $container->get('entity_field.manager'),
      $container->get('soft_translations.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function overview(RouteMatchInterface $route_match, $entity_type_id = NULL) {
    $build = parent::overview($route_match, $entity_type_id);

    /** @var \Drupal\Core\Entity\ContentEntityInterface $entity */
    $entity = $route_match->getParameter($entity_type_id);

    // Add support for the TMGMT module, which subclasses this to add their
    // own form elements.
    $tmgmt_modified = FALSE;
    if ($this->moduleHandler()->moduleExists('tmgmt_content')
      && class_exists('Drupal\tmgmt_content\Form\ContentTranslateForm')) {
      if (\Drupal::entityTypeManager()->getAccessControlHandler('tmgmt_job')->createAccess()) {
        $build = \Drupal::formBuilder()->getForm('Drupal\tmgmt_content\Form\ContentTranslateForm', $build);
        $tmgmt_modified = TRUE;
      }
    }
    // Inject an extra operation to allow splitting a translation.
    if ($tmgmt_modified) {
      $this->injectSplitOffOperationWithTmgmt($build, $entity);
    }
    else {
      $this->injectSplitOffOperation($build, $entity);
    }

    // If this doesn't have soft-translations, there's nothing additional to
    // do here.
    assert($entity instanceof TranslatableInterface);
    $rows = $this->getSoftTranslationsTableRows($entity);
    if (empty($rows)) {
      return $build;
    }

    // Add an additional table for the soft-translations, with a descriptive
    // title + explanation.
    $text = <<<MARKUP
<h2 style="margin-top:60px">{$this->t('Related Localized Content (Soft Translations)')}</h2>
<p>{$this->t('This page has other content that also exists at the same path in a different language. These pages are not one-to-one translations, but likely contain similar content that is localized to a particular region/language.')}</p>
MARKUP;

    $build['soft_translations_title'] = [
      '#type' => 'inline_template',
      '#template' => $text,
      '#weight' => 10,
    ];

    $build['soft_translations_table'] = [
      '#type' => 'table',
      '#header' => [
        $this->t('Language'),
        $this->t('Title'),
        $this->t('Status'),
        $this->t('Operations'),
      ],
      '#rows' => $rows,
      '#weight' => 20,
      '#attributes' => [
        'class' => ['soft-translations-overview-table'],
      ],
    ];

    return $build;
  }

  /**
   * Returns an array of soft translation rows.
   *
   * @param \Drupal\Core\Entity\TranslatableInterface $entity
   *   The entity we are working with.
   *
   * @return array
   *   The $rows array suitable to be used in the table render array.
   */
  protected function getSoftTranslationsTableRows(TranslatableInterface $entity) {
    $rows = [];

    $soft_translations = $this->softTranslationsManager->getSoftTranslations($entity, TRUE);
    foreach ($soft_translations as $translation) {
      $is_published = $this->t('N/A');
      if ($translation instanceof EntityPublishedInterface) {
        $is_published = $translation->isPublished() ? $this->t('Published') : $this->t('Unpublished');
      }
      $rows[] = [
        $translation->language()->getName(),
        $translation->toLink(),
        $is_published,
        [
          'data' => [
            '#type' => 'operations',
            '#links' => $this->entityTypeManager->getListBuilder($translation->getEntityTypeId())->getOperations($translation),
          ],
        ],
      ];
    }

    return $rows;
  }

  /**
   * Injects a "Split Off" operation when the table is a TMGMT table.
   *
   * @param array $build
   *   The build array, modified by reference.
   *
   * @param \Drupal\Core\Entity\TranslatableInterface $entity
   *   The entity we are dealing with.
   */
  protected function injectSplitOffOperationWithTmgmt(array &$build, TranslatableInterface $entity) {
    $entity_type_id = $entity->getEntityTypeId();
    // Note: This works as long as TMGMT table structure keeps the same.
    if (!empty($build['languages']['#options'])) {
      foreach ($build['languages']['#options'] as $langcode => $row) {
        // If the entity doesn't have a translation in this langcode, we can
        // just skip this row.
        if (!$entity->hasTranslation($langcode)) {
          continue;
        }
        // Let's try to simplify everyone's lives by only exposing this action
        // to non-original languages (i.e. only on translations of something
        // else).
        if ($entity->getUntranslated()->language()->getId() == $langcode) {
          continue;
        }
        // Make this column-agnostic in case TMGMT changes the position of the
        // options column in the future.
        foreach ($row as $pos => $column) {
          if (is_array($column) && !empty($column['data']['#type']) && $column['data']['#type'] === 'operations') {
            // Add our additional link here.
            $build['languages']['#options'][$langcode][$pos]['data']['#links']['soft-translations-split'] = [
              'url' => Url::fromRoute("entity.$entity_type_id.soft_translations_split", [
                $entity_type_id => $entity->id(),
                'entity_type' => $entity_type_id,
                'language' => $langcode,
              ]),
              'language' => $this->languageManager()->getDefaultLanguage(),
              'title' => $this->t('Split off'),
            ];
          }
        }
      }
    }
  }

  /**
   * Injects a "Split Off" operation into the translations table.
   *
   * @param array $build
   *   The build array, modified by reference.
   *
   * @param \Drupal\Core\Entity\TranslatableInterface $entity
   *   The entity we are dealing with.
   */
  protected function injectSplitOffOperation(array &$build, TranslatableInterface $entity) {
    $entity_type_id = $entity->getEntityTypeId();
    if (empty($build['content_translation_overview']['#rows'])) {
      return;
    }
    // Unfortunately there doesn't seem to be a quick-and-easy way of detecting
    // the langcode of each row. We'll match the language name from the first
    // column, which will also mean we'll skip the original language, where
    // the "(Original language)" suffix is added. This is something we want,
    // since we don't want to allow splitting off the default language.
    $languages = $this->languageManager()->getLanguages();
    $language_names = array_map(function ($item) {
      return $item->getName();
    }, $languages);
    $language_names = array_flip($language_names);
    foreach ($build['content_translation_overview']['#rows'] as &$row) {
      if (is_array($row)) {
        $language_name = (string) $row[0];
        if (array_key_exists($language_name, $language_names)) {
          $langcode = $language_names[$language_name];
        }
        else {
          // We don't want to add the split operation into this, which is
          // probably the original (untranslated) row.
          continue;
        }
        // If the entity doesn't have a translation in this langcode, we can
        // just skip this row.
        if (!$entity->hasTranslation($langcode)) {
          continue;
        }
        foreach ($row as $pos => $column) {
          if (is_array($column) && !empty($column['data']['#type']) && $column['data']['#type'] === 'operations') {
            // Add our additional link here.
            $row[$pos]['data']['#links']['soft-translations-split'] = [
              'url' => Url::fromRoute("entity.$entity_type_id.soft_translations_split", [
                $entity_type_id => $entity->id(),
                'entity_type' => $entity_type_id,
                'language' => $langcode,
              ]),
              'language' => $this->languageManager()->getDefaultLanguage(),
              'title' => $this->t('Split off'),
            ];
          }
        }
      }
    }
  }

}
