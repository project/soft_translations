<?php

namespace Drupal\soft_translations\Routing;

use Drupal\content_translation\ContentTranslationManager;
use Drupal\content_translation\ContentTranslationManagerInterface;
use Drupal\Core\Routing\RouteSubscriberBase;
use Drupal\Core\Routing\RoutingEvents;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

/**
 * Subscriber to alter other routes.
 */
class SoftTranslationsRouteSubscriber extends RouteSubscriberBase {

  /**
   * The content translation manager.
   *
   * @var \Drupal\content_translation\ContentTranslationManagerInterface
   */
  protected $contentTranslationManager;

  /**
   * Constructs a ContentTranslationRouteSubscriber object.
   *
   * @param \Drupal\content_translation\ContentTranslationManagerInterface $content_translation_manager
   *   The content translation manager.
   */
  public function __construct(ContentTranslationManagerInterface $content_translation_manager) {
    $this->contentTranslationManager = $content_translation_manager;
  }

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    // Look for routes that use  ContentTranslationController and change it
    // to our subclass. Override both content_translation and tmgmt_content
    // route definitions.
    foreach ($collection as $route) {
      if ($route->getDefault('_controller') == '\Drupal\content_translation\Controller\ContentTranslationController::overview'
        || $route->getDefault('_controller') == '\Drupal\tmgmt_content\Controller\ContentTranslationControllerOverride::overview') {
        $route->setDefault('_controller', '\Drupal\soft_translations\Controller\SoftTranslationsContentTranslationControllerOverride::overview');
      }
    }

    foreach ($this->contentTranslationManager->getSupportedEntityTypes() as $entity_type_id => $entity_type) {
      // Inherit admin route status from edit route, if exists.
      $is_admin = FALSE;
      $route_name = "entity.$entity_type_id.edit_form";
      if ($edit_route = $collection->get($route_name)) {
        $is_admin = (bool) $edit_route->getOption('_admin_route');
      }

      $load_latest_revision = ContentTranslationManager::isPendingRevisionSupportEnabled($entity_type_id);

      if ($entity_type->hasLinkTemplate('soft-translations:split')) {
        $route = new Route(
          $entity_type->getLinkTemplate('soft-translations:split'),
          [
            '_entity_form' => $entity_type_id . '.soft-translations-split',
            'language' => NULL,
            '_title' => 'Split Off',
            'entity_type_id' => $entity_type_id,
          ],
          [
            '_permission' => 'split off soft translations',
          ],
          [
            'parameters' => [
              'language' => [
                'type' => 'language',
              ],
              $entity_type_id => [
                'type' => 'entity:' . $entity_type_id,
                'load_latest_revision' => $load_latest_revision,
              ],
            ],
            '_admin_route' => $is_admin,
          ]
        );
        $collection->add("entity.$entity_type_id.soft_translations_split", $route);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events = parent::getSubscribedEvents();
    // We want to come after core (-100) and TMGMT (-211).
    $events[RoutingEvents::ALTER] = ['onAlterRoutes', -312];
    return $events;
  }

}
