<?php

namespace Drupal\soft_translations;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\TranslatableInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Path\PathValidatorInterface;
use Drupal\path_alias\AliasManagerInterface;

/**
 * A helper class to handle soft translations.
 */
class SoftTranslationsManager {

  /**
   * The Alias Manger service.
   *
   * @var \Drupal\path_alias\AliasManagerInterface
   */
  protected $aliasManager;

  /**
   * The Language Manager service.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * The Path Validator service.
   *
   * @var \Drupal\Core\Path\PathValidatorInterface
   */
  protected $pathValidator;

  /**
   * The Entity Type Manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Class constructor.
   *
   * @param \Drupal\path_alias\AliasManagerInterface $alias_manager
   *   The alias manager service.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager service.
   * @param \Drupal\Core\Path\PathValidatorInterface $path_validator
   *   The path validator service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(AliasManagerInterface $alias_manager, LanguageManagerInterface $language_manager, PathValidatorInterface $path_validator, EntityTypeManagerInterface $entity_type_manager) {
    $this->aliasManager = $alias_manager;
    $this->languageManager = $language_manager;
    $this->pathValidator = $path_validator;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Retrieve soft translations of a given entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to query soft translations for.
   * @param bool $return_entities
   *   (optional) Whether to return the objects of the "translations",
   *   instead of their respective paths (i.e. /node/123). Defaults to FALSE.
   *
   * @return string[]|\Drupal\Core\Entity\EntityInterface
   *   An array of internal entity paths, keyed by their langcode. If
   *   $return_entities is TRUE, values will be the respective entity objects,
   *   instead of path strings.
   */
  public function getSoftTranslations(EntityInterface $entity, $return_entities = FALSE) {
    $paths = [];

    // Non-translatable or unsaved entities by definition can't have
    // soft translations.
    if (!($entity instanceof TranslatableInterface) || $entity->isNew() || !$entity->hasLinkTemplate('canonical')) {
      return $paths;
    }

    // @todo Expand this so we can use other detection methods as well.
    // Case 1: Alias-based.
    $entity_path = '/' . $entity->toUrl()->getInternalPath();
    $original_alias = $this->aliasManager->getAliasByPath($entity_path, $entity->language()->getId());
    $languages = $this->languageManager->getLanguages();
    // Remove the language of the original entity, since there's no point in
    // returning itself within the results.
    unset($languages[$entity->language()->getId()]);
    foreach (array_keys($languages) as $langcode) {
      $path = $this->aliasManager->getPathByAlias($original_alias, $langcode);
      // The manager returns the original alias if no path is found.
      if ($path !== $original_alias) {
        // Before including this path, make sure it refers to a different entity
        // than the one passed in.
        if ($path != $entity_path) {
          $paths[$langcode] = $path;
        }
      }
    }

    if (!$return_entities || empty($paths)) {
      return $paths;
    }

    $entity_type_id = $entity->getEntityTypeId();
    $storage = $this->entityTypeManager->getStorage($entity_type_id);
    $entities = array_filter(array_map(function ($path) use ($storage, $entity_type_id) {
      $url = $this->pathValidator->getUrlIfValidWithoutAccessCheck($path);
      if ($url && $url->isRouted() && $url->getRouteName() === "entity.{$entity_type_id}.canonical") {
        $route_parameters = $url->getRouteParameters();
        $id = $route_parameters[$entity_type_id];
        return $storage->load($id);
      }
      return FALSE;
    }, $paths));

    // At the risk of being too cautious, make sure we are returning loaded
    // entities in the language that corresponds to each langcode. In practice
    // we expect these soft translations to have their default language the
    // same as the translated language, but there's nothing preventing editors
    // from doing otherwise.
    foreach ($entities as $langcode => $entity) {
      if ($entity->hasTranslation($langcode)) {
        $entities[$langcode] = $entity->getTranslation($langcode);
      }
    }

    return $entities;
  }

}
